<div class="form-group">
	<div class="col">
		<?php if($this->session->flashdata('status') && $this->session->flashdata('info') && $this->session->flashdata('colorInfo')):?>
		<div class="alert alert-<?= $this->session->flashdata('colorInfo') ?> alert-dismissible fade show" role="alert">
			Periode <strong><?= $this->session->flashdata('status') ?></strong>
			<?= $this->session->flashdata('info') ?>!
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<?php endif;?>
	</div>
</div>
<div class="form-group">
	<div class="container-fluid">
		<h1 class="h3 mb-4 text-gray-800">Periode</h1>
	</div>
	<div class="col">
		<div class="form-group">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalTambahPeriode">
				<i class="fas fa-plus"></i> Tambah Periode
			</button>
		</div>
	</div>
	<div class="col-lg-7">
		<table class="table table-hover table-responsive-lg">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Tahun Ajaran</th>
					<th scope="col">Semester</th>
				</tr>
			</thead>
			<tbody>
				<?php for ($i=0; $i < count($all_periode); $i++):?>
				<tr>
					<th><?=$i+1; ?></th>
					<td><?=$all_periode[$i]['tahun_ajaran']; ?></td>
					<?php if($all_periode[$i]['semester'] == 1){?>
					<td>Ganjil</td>
					<?php }else{?>
					<td>Genap</td>
					<?php }?>
				</tr>
				<?php endfor; ?>
			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalTambahPeriode" tabindex="-1" role="dialog" aria-labelledby="modalTambahPeriodeLabel"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modalTambahPeriodeLabel">Tambah Periode</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<h6>Catatan : Isi kolom sesuai dengan format yang ditentukan.</h6>
				<form action="<?= base_url('periode/tambah')?>" method="post">
					<div class="modal-body">
						<div class="form-group"><label>Tahun Ajaran</label><input type="text" class="form-control"
								id="tahun" name="tahun" placeholder="ex : 2015/2016" maxlength="9" required></div>
						<div class="form-group"><label>Semester</label>
							<select class="custom-select" name="semester" id="semester">
								<option value="1" selected>Ganjil</option>
								<option value="2">Genap</option>
							</select>
						</div>
					</div>
					<div class="modal-footer"><button type="submit" class="btn btn-primary">Tambah</button></div>
				</form>
			</div>
		</div>
	</div>
</div>
