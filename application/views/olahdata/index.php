<div class="form-group">
	<div class="container-fluid">
		<h1 class="h3 mb-4 text-gray-800">Olah Data</h1>
		<?php if($this->session->flashdata('error')){?>
			<div class="alert alert-danger alert-dismissible fade show col-lg-7" role="alert">
				<?= $this->session->flashdata('error') ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		<?php }?>
		<div class="row">
			<div class="col-lg-2 custom-file">
				<form method="post" action="<?= base_url('olahdata/upload')?>" enctype="multipart/form-data">
					<input type="file" name="excelFile" class="form-control-file">
			</div>
			<div class="col">
				<button type="submit" id="btnSubmit" class="btn btn-primary"><i class="fas fa-eye"></i> Tampilkan</button>
				
				</form>
			</div>
		</div><br>
		<div class="row">
			<div class="col table-responsive-lg"id="tableContent">
				<table class="table table-hover">
					<?php if(isset($dataTable)){?>
						<thead>
							<tr>
								<th>#</th>
								<th>Kode</th>
								<th>Matakuliah</th>
								<th>NPM</th>
								<th>Asisten</th>
								<th>P. Materi</th>
								<th>M. Praktikum</th>
								<th>P. Speaking</th>
								<th>Kedisiplinan</th>
							</tr>
						</thead>
						<tbody>
							<?php for ($i=0; $i <count($dataTable) ; $i++):?>
							<tr>
								<td><?= $i+1?></td>
								<td><?= $dataTable[$i]['kode_makul']?></td>
								<td><?= $dataTable[$i]['nama_makul']?></td>
								<td><?= $dataTable[$i]['npm']?></td>
								<td><?= $dataTable[$i]['nama_asisten']?></td>
								<td><?= $dataTable[$i]['penguasaan_materi']?></td>
								<td><?= $dataTable[$i]['membimbing_praktikum']?></td>
								<td><?= $dataTable[$i]['public_speaking']?></td>
								<td><?= $dataTable[$i]['kedisiplinan']?></td>
							</tr>
							<?php endfor?>
						</tbody>
					<?php }else{ ?>
						<thead>
							<tr>
								<th>#</th>
								<th>Kode Matakuliah</th>
								<th>Nama Matakuliah</th>
								<th>NPM</th>
								<th>Nama Asisten</th>
								<th>Penguasaan Materi</th>
								<th>Membimbing Praktikum</th>
								<th>Public Speaking</th>
								<th>Kedisiplinan</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="9">Tidak ada data yang ditampilkan</td>
							</tr>
						</tbody>
					<?php } ?>
				</table>
			</div>
		</div>
		<div id="btnProses">
			<a href="" class="btn btn-primary" data-toggle="modal" data-target="#prosesModal"><i class="fas fa-cogs"></i> Proses</a>
		</div>
		
	</div>
</div>