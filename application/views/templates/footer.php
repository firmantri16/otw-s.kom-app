</div>
<footer class="sticky-footer bg-white">
	<div class="container my-auto">
		<div class="copyright text-center my-auto">
			<span>Dikembangkan oleh <b><a href="<?= base_url('fadev')?>">FA Developer</a></b></span>
		</div>
	</div>
</footer>
</div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
	<i class="fas fa-angle-up"></i>
</a>
<!-- Logout Modal -->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Yakin ingin keluar?</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">Anda harus login kembali apabila sudah keluar.</div>
			<div class="modal-footer">
				<button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fas fa-hand-point-left"></i>
					Kembali</button>
				<a class="btn btn-primary" href="<?= base_url('auth/logout')?>"><i class="fas fa-sign-out-alt"></i>
					Keluar</a>
			</div>
		</div>
	</div>
</div>
<!-- Proses Modal -->
<div class="modal fade" id="prosesModal" tabindex="-1" role="dialog" aria-labelledby="prosesModalLabel"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="prosesModalLabel">Silahkan isi periode olahdata.</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-6">
						<form action="<?= base_url('olahdata/proses/')?>" method="post">
							<select name="tahun_ajaran" class="custom-select">
								<option selected>Tahun Ajaran</option>
								<?php for ($i=0; $i < count($tahunAjaran); $i++):?>
								<option value="<?= $tahunAjaran[$i]['tahun_ajaran']?>">
									<?= $tahunAjaran[$i]['tahun_ajaran']?></option>
								<?php endfor?>
							</select>
					</div>
					<div class="col-lg-6">
						<select name="semester" class="custom-select">
							<option selected>Semester</option>
							<option value="1">Ganjil</option>
							<option value="2">Genap</option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" href=""><i class="fas fa-cogs"></i>
					Proses</button>
				</form>
			</div>
		</div>
	</div>
</div>
<script src="<?= base_url();?>/vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url();?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url();?>/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="<?= base_url();?>/vendor/js/sb-admin-2.min.js"></script>
</body>

</html>
