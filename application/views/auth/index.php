<br><br><br>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-lg-10">
			<?php if($this->session->flashdata('statusLogin') && $this->session->flashdata('colorInfo')):?>
			<div class="alert alert-<?= $this->session->flashdata('colorInfo') ?> alert-dismissible fade show"
				role="alert">
				Login <strong><?= $this->session->flashdata('statusLogin') ?></strong>! Periksa kembali NPM dan Password
				Anda.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php endif;?>
		</div>
		<div class="col-xl-10 col-lg-12 col-md-9">
			<div class="card o-hidden border-0 shadow-lg my-5">
				<div class="card-body p-0">
					<div class="row">
						<div class="col-lg-6 d-none d-lg-block">
							<img src="<?= base_url('vendor/img/foto_fa.jpg'); ?>" width="455" height="410" alt="">
						</div>
						<div class="col-lg-6">
							<div class="p-5">
								<div class="text-center">
									<h1 class="h4 text-gray-900 mb-4">Selamat Datang di Forum Asisten</h1>
								</div>
								<form class="user" action="<?= base_url('auth/login');?>" method="POST">
									<div class="form-group">
										<input type="text" class="form-control form-control-user" name="npm"
											id="exampleInputEmail" placeholder="NPM">
									</div>
									<div class="form-group">
										<input type="password" class="form-control form-control-user" name="password"
											id="exampleInputPassword" placeholder="Kata sandi">
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-primary btn-user btn-block"><i
												class="fas fa-sign-in-alt"></i> Login</button>
									</div>
								</form>
								<hr>
								<br>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
