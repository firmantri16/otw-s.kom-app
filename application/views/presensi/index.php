<div class="form-group">
	<div class="container-fluid">
		<h1 class="h3 mb-4 text-gray-800">Presensi</h1>
		<?php if($this->session->flashdata('error')){?>
			<div class="alert alert-danger alert-dismissible fade show col-lg-8" role="alert"><b>
				<?= $this->session->flashdata('error') ?></b>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		<?php }?>
		<div class="row">
			<div class="col-lg-2 custom-file">
				<form method="post" action="<?= base_url('presensi/upload')?>" enctype="multipart/form-data">
					<input type="file" name="excelFile" class="form-control-file">
			</div>
			<div class="col">
				<button type="submit" id="btnSubmit" class="btn btn-primary"><i class="fas fa-eye"></i> Tampilkan</button>
				
				</form>
			</div>
		</div><br>
		<div class="row">
			<div class="col table-responsive-lg"id="tableContent">
				<table class="table table-hover">
					<?php if(isset($dataTable)){?>
						<thead>
							<tr>
								<th>#</th>
								<th>NPM</th>
								<th>Nama Asisten</th>
								<th>Matakuliah</th>
								<th>Jurusan</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<?php for ($i=0; $i <count($dataTable) ; $i++):?>
							<tr>
								<td><?= $i+1?></td>
								<td><?= $dataTable[$i]['npm']?></td>
								<td><?= $dataTable[$i]['nama_asisten']?></td>
								<td><?= $dataTable[$i]['nama_makul']?></td>
								<td><?= $dataTable[$i]['jurusan']?></td>
								<td><?= $dataTable[$i]['jumlah_presensi']?></td>
							</tr>
							<?php endfor?>
						</tbody>
					<?php }else{ ?>
						<thead>
							<tr>
                                <th>#</th>
								<th>NPM</th>
								<th>Nama Asisten</th>
								<th>Matakuliah</th>
								<th>Jurusan</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="9">Tidak ada data yang ditampilkan</td>
							</tr>
						</tbody>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
</div>