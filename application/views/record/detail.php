<div class="form-group">
	<div class="container-fluid">
		<h1 class="h3 mb-4 text-gray-800">Detail Record</h1>
		<div class="row ">
			<div class="col-lg-12 table-responsive-lg">
				<div id="periode">
					<h6 class="text-gray-700"><?= $data_periode["tahun"]?> | 
						<?php if($data_periode["semester"] == 1){?>
							Ganjil
						<?php }else{?>
							Genap
						<?php }?>
					</h6>
				</div>
				<?php if($daftar_record == "Tidak ada data"){?>
				<br>
				<div class="alert alert-danger col-lg-5" role="alert">
					Belum ada data pada periode ini.
				</div>
				<?php }else{?>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>NPM</th>
							<th>Nama Asisten</th>
							<th>Kode Matakuliah</th>
							<th>Matakuliah</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php for ($i=0; $i < sizeof($daftar_record); $i++) :?>
						<tr>
							<th><?= $i+1?></th>
							<td><?= $daftar_record[$i]["npm"]?></td>
							<td><?= $daftar_record[$i]["nama_asisten"]?></td>
							<td><?= $daftar_record[$i]["kode_makul"]?></td>
							<td><?= $daftar_record[$i]["nama_makul"]?></td>
							<td><a href="<?= base_url("record/all/".$data_periode["id_periode"]."/".$daftar_record[$i]["kode_makul"])?>"
									class="badge badge-pill badge-danger"><i class="fas fa-eye"></i> Lihat Semua</a></td>
						</tr>
						<?php endfor?>
					</tbody>
				</table>
				<?php }?>
			</div>
		</div>
	</div>
</div>
