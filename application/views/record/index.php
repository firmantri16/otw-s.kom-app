<div class="form-group">
	<div class="container-fluid">
		<h1 class="h3 mb-4 text-gray-800">Record</h1>
		<div class="row ">
			<?php if($this->session->flashdata('status_download')){?>
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
				<strong>Arsip <?= $this->session->flashdata('status_download') ?> diunduh!</strong> Tidak ada data yang tersedia
				pada periode ini.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php }?>
			<div class="col-lg-8 table-responsive-lg">
				<table class="table table-hover">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Tahun Ajaran</th>
							<th scope="col">Semester</th>
							<th scope="col">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php for ($i=0; $i < sizeof($daftar_periode); $i++):?>
						<tr>
							<th><?= $i+1?></th>
							<td><?= $daftar_periode[$i]["tahun_ajaran"]?></td>
							<?php if($daftar_periode[$i]["semester"] == 1){?>
							<td>Ganjil</td>
							<?php }else{?>
							<td>Genap</td>
							<?php }?>
							<td>
								<a href="<?= base_url("record/detail/".$daftar_periode[$i]["tahun_ajaran"]."/".$daftar_periode[$i]["semester"]."/")?>"
									class="badge badge-pill badge-danger"><i class="fas fa-eye"></i> Detail</a>
								<a href="<?= base_url("record/downloadRecord/".$daftar_periode[$i]["tahun_ajaran"]."/".$daftar_periode[$i]["semester"]."/")?>"
									class="badge badge-pill badge-success"><i class="fas fa-file-download"></i>
									Download</a>
							</td>
						</tr>
						<?php endfor?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
