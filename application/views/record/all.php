<div class="form-group">
	<div class="container-fluid">
		<h1 class="h3 mb-4 text-gray-800">Detail Record</h1>
		<div class="row ">
			<div class="col-lg-12 table-responsive-lg">
				<div id="periode">
					<h6 class="text-gray-700"><?= $data_periode[0]["tahun_ajaran"]?> | 
						<?php if($data_periode[0]["semester"] == 1){?>
							Ganjil
						<?php }else{?>
							Genap
						<?php }?>
					</h6>
				</div>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>NPM</th>
							<th>Nama Asisten</th>
							<th>Kode Matakuliah</th>
							<th>Matakuliah</th>
							<th>Nilai</th>
						</tr>
					</thead>
					<tbody>
						<?php for ($i=0; $i < sizeof($detail_record); $i++) :?>
						<tr>
							<th><?= $i+1?></th>
							<td><?= $detail_record[$i]["npm"]?></td>
							<td><?= $detail_record[$i]["nama_asisten"]?></td>
							<td><?= $detail_record[$i]["kode_makul"]?></td>
							<td><?= $detail_record[$i]["nama_makul"]?></td>
							<td><?= $detail_record[$i]["nilai"]?></td>
						</tr>
						<?php endfor?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
