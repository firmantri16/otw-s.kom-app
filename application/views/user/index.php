<div class="form-group">
	<div class="col">
		<?php if($this->session->flashdata('status') && $this->session->flashdata('info') && $this->session->flashdata('colorInfo')):?>
		<div class="alert alert-<?= $this->session->flashdata('colorInfo') ?> alert-dismissible fade show" role="alert">
			Data pengguna <strong><?= $this->session->flashdata('status') ?></strong>
			<?= $this->session->flashdata('info') ?>!
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<?php endif;?>
		<div class="card col-lg-6">
			<div class="row">
				<div class="col-lg-5"><br><img class="card-img-top"
						src="<?= base_url('vendor/img/user profile.png'); ?>"></div>
				<div class="col-lg-6">
					<div class="card-body">
						<h5 class="card-title text-gray-800">Pengurus FA</h5>
						<ul class="list-group list-group-flush">
							<li class="list-group-item"><i class="fas fa-signature"></i>
								<?=$current_user[0]['fullname']?></li>
							<li class="list-group-item"><i class="fas fa-id-badge"></i> <?=$current_user[0]['npm']?>
							</li>
							<li class="list-group-item"><i class="fas fa-info-circle"></i>
								<?=$current_user[0]['konsentrasi']?></li><br>
							<div class="form-group"><button type="button" class="btn btn-warning" data-toggle="modal"
									data-target="#editUserModal"><i class="fas fa-user-edit"></i> Edit Profile </button>
							</div>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><br>
<div class="form-group">
	<div class="container-fluid">
		<h1 class="h3 mb-4 text-gray-800">Daftar Pengguna</h1>
	</div>
	<div class="col">
		<div class="form-group"><button type="button" class="btn btn-primary" data-toggle="modal"
				data-target="#addUserModal"><i class="fas fa-user-plus"></i> Tambah Pengguna </button></div>
	</div>
	<div class="col">
		<table class="table table-hover table-responsive-lg">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">NPM</th>
					<th scope="col">Nama Lengkap</th>
					<th scope="col">Konsentrasi</th>
					<th scope="col">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php for ($i=0; $i < count($all_user); $i++):?>
				<tr>
					<th><?=$i+1; ?></th>
					<td><?=$all_user[$i]['npm']; ?></td>
					<td><?=$all_user[$i]['fullname']; ?></td>
					<td><?=$all_user[$i]['konsentrasi']; ?></td>
					<td><a href="<?= base_url('user/delete/') . $all_user[$i]['npm']?>"
							class="badge badge-pill badge-danger"><i class="fas fa-eraser"></i> Delete</a></td>
				</tr>
				<?php endfor; ?></tbody>
		</table>
	</div>
</div>
<!-- Modal Add User -->
<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="addUserModalLabel"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-gray-800" id="addUserModalLabel">Kolom tidak boleh kosong !</h5><button
					type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;
					</span></button>
			</div>
			<form action="<?= base_url('user/add')?>" method="post">
				<div class="modal-body">
					<div class="form-group"><label>NPM</label><input type="text" class="form-control" id="npm"
							name="npm" placeholder="15.11.xxxx" maxlength="10" required></div>
					<div class="form-group"><label>Password</label><input type="password" class="form-control"
							id="password" name="password" placeholder="Password" required></div>
					<div class="form-group"><label>Fullname</label><input type="text" class="form-control" id="fullname"
							name="fullname" placeholder="Fullname" required></div>
					<div class="form-group"><label>Konsentrasi</label><input type="text" class="form-control"
							id="konsentrasi" name="konsentrasi" placeholder="Konsentrasi" required></div>
				</div>
				<div class="modal-footer"><button type="submit" class="btn btn-primary">Tambah</button></div>
			</form>
		</div>
	</div>
</div>
<!-- Modal Edit User -->
<div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="editUserModalLabel"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-gray-800" id="editUserModalLabel">Edit Profile</h5><button type="button"
					class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;
					</span></button>
			</div>
			<form action="<?= base_url('user/edit')?>" method="post">
				<div class="modal-body">
					<div class="form-group"><label>User NPM : <?=$current_user[0]['npm'] ?></label></div>
					<div class="form-group"><label>Password</label><input type="password" class="form-control"
							id="password" name="password" placeholder="Password"
							value="<?= $current_user[0]['password'] ?>" required></div>
					<div class="form-group"><label>Fullname</label><input type="text" class="form-control" id="fullname"
							name="fullname" placeholder="Fullname" value="<?= $current_user[0]['fullname'] ?>" required>
					</div>
					<div class="form-group"><label>Konsentrasi</label><input type="text" class="form-control"
							id="konsentrasi" name="konsentrasi" value="<?= $current_user[0]['konsentrasi'] ?>"
							placeholder="Konsentrasi" required></div>
				</div>
				<div class="modal-footer"><button type="submit" class="btn btn-primary">Save Changes</button></div>
			</form>
		</div>
	</div>
</div>
