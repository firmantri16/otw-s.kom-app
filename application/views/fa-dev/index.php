<div class="form-group">
	<div class="col">
	<h1 class="h3 mb-4 text-gray-800">Developer</h1>
		<div class="card col-lg-5">
			<div class="row">
				<div class="col-lg-6">
					<img class="card-img-top" src="<?= base_url('vendor/img/firman_fadev.jpg')?>"
						alt="Firman Tri Anggara">
				</div>
				<div class="card-body">
					<h5 class="card-title text-gray-800">Pengurus FA</h5>
					<ul class="list-group list-group-flush">
						<li class="list-group-item"><i class="fas fa-signature"></i> Firman Tri Anggara</li>
						<li class="list-group-item"><i class="fas fa-id-badge"></i> 15.11.8692</li>
						<li class="list-group-item"><i class="fas fa-info-circle"></i> Web Programming</li><br>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>