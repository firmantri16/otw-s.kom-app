-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Jul 2019 pada 13.06
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fa_database`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_record`
--

CREATE TABLE `detail_record` (
  `id_detail_record` int(11) NOT NULL,
  `kode_makul` varchar(30) NOT NULL,
  `nama_makul` varchar(100) NOT NULL,
  `npm` varchar(10) NOT NULL,
  `nama_asisten` varchar(30) NOT NULL,
  `penguasaan_materi` float NOT NULL,
  `membimbing_praktikum` float NOT NULL,
  `public_speaking` float NOT NULL,
  `kedisiplinan` float NOT NULL,
  `nilai` float NOT NULL,
  `id_periode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `olahdata`
--

CREATE TABLE `olahdata` (
  `id_olahdata` int(11) NOT NULL,
  `kode_makul` varchar(30) NOT NULL,
  `nama_makul` varchar(100) NOT NULL,
  `npm` varchar(10) NOT NULL,
  `nama_asisten` varchar(30) NOT NULL,
  `penguasaan_materi` float NOT NULL,
  `membimbing_praktikum` float NOT NULL,
  `public_speaking` float NOT NULL,
  `kedisiplinan` float NOT NULL,
  `nilai` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `periode`
--

CREATE TABLE `periode` (
  `id_periode` int(11) NOT NULL,
  `tahun_ajaran` varchar(30) NOT NULL,
  `semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `periode`
--

INSERT INTO `periode` (`id_periode`, `tahun_ajaran`, `semester`) VALUES
(9, '2015/2016', 1),
(10, '2015/2016', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `presensi`
--

CREATE TABLE `presensi` (
  `id_presensi` int(11) NOT NULL,
  `npm` varchar(30) NOT NULL,
  `nama_asisten` varchar(30) NOT NULL,
  `nama_makul` varchar(100) NOT NULL,
  `jurusan` varchar(100) NOT NULL,
  `jumlah_presensi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `presensi`
--

INSERT INTO `presensi` (`id_presensi`, `npm`, `nama_asisten`, `nama_makul`, `jurusan`, `jumlah_presensi`) VALUES
(824, '17.11.1152', 'AGNES LUCKY REBECCA', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 14),
(825, '17.11.1123', 'ALFIN ANDIKA PRATAMA', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 17),
(826, '17.11.1132', 'ANANG WAHYU PRADANA', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 6),
(827, '17.11.1501', 'ANDHY PANCA SAPUTRA', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 5),
(828, '17.11.1121', 'AULIA KHANSA DAMARANI', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 14),
(829, '17.61.0105', 'BAYU PERMANA SEJATI', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 13),
(830, '17.11.1539', 'BRAYEN BAKARA', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 14),
(831, '17.61.0110', 'DZULFIKAR ALI MASKUR', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 8),
(832, '17.11.1337', 'EDWIN RAHMAD TOHA', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 8),
(833, '17.11.1500', 'FAIZAL FAKHRI IRFANI', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 13),
(834, '16.11.0780', 'FUAD HABIB NURRAHMAN', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 9),
(835, '16.11.0625', 'HANIF AISYAH', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 13),
(836, '17.11.1485', 'KAHFI BAIDOWI', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 7),
(837, '17.11.1356', 'MAHMUD ZAKARIYA ALFAROZY', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 5),
(838, '17.11.1102', 'MUHSON NAWAWI', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 16),
(839, '17.11.1701', 'NANDA ATIKA HARIANI', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 16),
(840, '17.11.1247', 'RIZKY NUR HIDAYATULLAH', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 7),
(841, '17.11.1653', 'RIZKY ROSYID HIDAYAT', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 8),
(842, '17.11.1452', 'RONY PERMADI', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 7),
(843, '17.11.1052', 'SYEKH ARPI AGENG', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 5),
(844, '16.11.0491', 'TIMUR DIAN RADHA SEJATI', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 13),
(845, '17.11.1725', 'VINSENSIUS DIMAS ABISTA', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 7),
(846, '17.11.1664', 'YAHYA SUGANDI.Y', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 9),
(847, '17.11.1692', 'syaima asy syahzan gomes', 'ALGORITMA DAN PEMROGRAMAN', 'S1 INFORMATIKA', 14);

-- --------------------------------------------------------

--
-- Struktur dari tabel `record`
--

CREATE TABLE `record` (
  `id_record` int(11) NOT NULL,
  `kode_makul` varchar(30) NOT NULL,
  `nama_makul` varchar(30) NOT NULL,
  `npm` varchar(30) NOT NULL,
  `nama_asisten` varchar(30) NOT NULL,
  `nilai` float NOT NULL,
  `id_periode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `temporary`
--

CREATE TABLE `temporary` (
  `id_temporary` int(11) NOT NULL,
  `kode_makul` varchar(30) NOT NULL,
  `nama_makul` varchar(100) NOT NULL,
  `npm` varchar(10) NOT NULL,
  `nama_asisten` varchar(30) NOT NULL,
  `penguasaan_materi` float NOT NULL,
  `membimbing_praktikum` float NOT NULL,
  `public_speaking` float NOT NULL,
  `kedisiplinan` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `temporary`
--

INSERT INTO `temporary` (`id_temporary`, `kode_makul`, `nama_makul`, `npm`, `nama_asisten`, `penguasaan_materi`, `membimbing_praktikum`, `public_speaking`, `kedisiplinan`) VALUES
(2560, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '16.11.0491', 'Timur Dian Radha Sejati', 343, 320, 320, 325),
(2561, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '16.11.0625', 'Hanif Aisyah', 275, 306, 271, 273),
(2562, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '16.11.0780', 'Fuad Habib Nurrahman', 331, 313, 319, 309),
(2563, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1052', 'Syekh Arpi Ageng', 209, 212, 207, 205),
(2564, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1102', 'Muhson Nawawi', 374, 370, 355, 363),
(2565, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1121', 'Aulia Khansa Damarani', 373, 363, 364, 360),
(2566, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1123', 'Alfin Andika Pratama', 292, 265, 275, 282),
(2567, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1132', 'Anang Wahyu Perdana', 227, 219, 209, 211),
(2568, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1152', 'Agnes Lucky Rebecca', 173, 155, 165, 156),
(2569, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1247', 'Rizky Nur Hidayatullah', 143, 131, 135, 137),
(2570, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1452', 'Rony Permadi', 181, 170, 176, 172),
(2571, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1485', 'Kahfi Baidowi', 162, 152, 155, 158),
(2572, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1500', 'Faizal Fakhri Irfani', 346, 309, 320, 325),
(2573, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1501', 'Andhy Panca Saputra', 163, 162, 159, 162),
(2574, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1539', 'Brayen Bakara', 380, 345, 348, 344),
(2575, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1653', 'Rizky Rosyid Hidayat', 185, 185, 175, 175),
(2576, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1664', 'YAHYA SUGANDI.Y', 225, 216, 208, 213),
(2577, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1692', 'Syaima Asy Syahzan Gomes', 122, 120, 116, 118),
(2578, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.11.1701', 'Nanda Atika Hariani', 385, 356, 361, 356),
(2579, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.61.0105', 'Bayu Permana sejati', 206, 208, 204, 201),
(2580, 'ST068', 'ALGORITMA DAN PEMROGRAMAN', '17.61.0110', 'Dzulfikar Ali Maskur', 203, 195, 196, 194);

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_cluster`
--

CREATE TABLE `temp_cluster` (
  `id_temp_cluster` int(11) NOT NULL,
  `nomor_data` int(11) NOT NULL,
  `cluster` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `temp_cluster`
--

INSERT INTO `temp_cluster` (`id_temp_cluster`, `nomor_data`, `cluster`) VALUES
(68357, 0, 1),
(68358, 1, 1),
(68359, 2, 1),
(68360, 3, 2),
(68361, 4, 1),
(68362, 5, 1),
(68363, 6, 1),
(68364, 7, 2),
(68365, 8, 2),
(68366, 9, 2),
(68367, 10, 2),
(68368, 11, 2),
(68369, 12, 1),
(68370, 13, 2),
(68371, 14, 1),
(68372, 15, 2),
(68373, 16, 2),
(68374, 17, 2),
(68375, 18, 1),
(68376, 19, 2),
(68377, 20, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `npm` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `fullname` varchar(30) NOT NULL,
  `konsentrasi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`npm`, `password`, `fullname`, `konsentrasi`) VALUES
('15.11.8692', 'firmantria10', 'Firman Tri Anggara', 'Pemrograman');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `detail_record`
--
ALTER TABLE `detail_record`
  ADD PRIMARY KEY (`id_detail_record`);

--
-- Indeks untuk tabel `olahdata`
--
ALTER TABLE `olahdata`
  ADD PRIMARY KEY (`id_olahdata`);

--
-- Indeks untuk tabel `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`id_periode`);

--
-- Indeks untuk tabel `presensi`
--
ALTER TABLE `presensi`
  ADD PRIMARY KEY (`id_presensi`);

--
-- Indeks untuk tabel `record`
--
ALTER TABLE `record`
  ADD PRIMARY KEY (`id_record`),
  ADD KEY `id_periode` (`id_periode`);

--
-- Indeks untuk tabel `temporary`
--
ALTER TABLE `temporary`
  ADD PRIMARY KEY (`id_temporary`);

--
-- Indeks untuk tabel `temp_cluster`
--
ALTER TABLE `temp_cluster`
  ADD PRIMARY KEY (`id_temp_cluster`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`npm`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `detail_record`
--
ALTER TABLE `detail_record`
  MODIFY `id_detail_record` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT untuk tabel `olahdata`
--
ALTER TABLE `olahdata`
  MODIFY `id_olahdata` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=786;

--
-- AUTO_INCREMENT untuk tabel `periode`
--
ALTER TABLE `periode`
  MODIFY `id_periode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `presensi`
--
ALTER TABLE `presensi`
  MODIFY `id_presensi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=848;

--
-- AUTO_INCREMENT untuk tabel `record`
--
ALTER TABLE `record`
  MODIFY `id_record` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT untuk tabel `temporary`
--
ALTER TABLE `temporary`
  MODIFY `id_temporary` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2581;

--
-- AUTO_INCREMENT untuk tabel `temp_cluster`
--
ALTER TABLE `temp_cluster`
  MODIFY `id_temp_cluster` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68378;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `record`
--
ALTER TABLE `record`
  ADD CONSTRAINT `record_ibfk_1` FOREIGN KEY (`id_periode`) REFERENCES `periode` (`id_periode`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
