<?php
defined('BASEPATH') or exit('No direct script access allowed');
//Controller merupakan penghubung antara Model dengan View.
class Auth extends CI_Controller
{
    public function index() //fungsi tampil
    {
        if($this->session->userdata('username') != ''){
            redirect(base_url('dashboard'));
        }else{
            //View
            $this->load->view('templates/auth_header');
            $this->load->view('auth/index');
            $this->load->view('templates/auth_footer');
        }
    }

    public function login()
    {
        $npm = $this->input->post('npm');
        $password = $this->input->post('password');
        //Model
        $this->load->model('Auth_model'); //Dashboard_model di load oleh Controller.
        $data = $this->Auth_model->verification($npm, $password);

        if($data[0]['fullname'] != ''){
            $session_data = array(
                'username' => $data[0]['npm'],
                'fullname' => $data[0]['fullname']
            );
            $this->session->set_userdata($session_data);
            //View -> redirect to controller Dashboard function index.
            redirect(base_url('dashboard'));
        }else{
            $this->session->set_flashdata('statusLogin', 'gagal');
            $this->session->set_flashdata('colorInfo', 'danger');
            
            redirect(base_url('auth'));
        }
    }
    
    public function logout(){
        if($this->session->userdata('username') != ''){
            $this->session->unset_userdata('username');
            $this->session->unset_userdata('fullname');
            redirect(base_url());
        }
    }
}
