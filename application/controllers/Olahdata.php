<?php
require_once('vendor/vendor/autoload.php');

defined('BASEPATH') or exit('No direct script access allowed');
//Controller merupakan penghubung antara Model dengan View.
class Olahdata extends CI_Controller
{
    public function __construct()
    {
            parent::__construct();
            $this->load->helper(array('form', 'url'));
            $this->load->model('Olahdata_model');
    }

    public function index()
    {
        if($this->session->userdata('username') != ''){
            $data['user'] = $this->session->userdata('username');
            $data['page_active'] = 'olahdata';
            $data['tahunAjaran'] = $this->Olahdata_model->tampilTahunAjaran();

            $this->load->view('templates/header', $data);
            $this->load->view('olahdata/index', $data);
            $this->load->view('templates/footer');
        }else{
            redirect(base_url());
        }
    }

    public function upload(){
        
        $filename = $_FILES['excelFile']['name'];
        // echo $filename;
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'xlsx';
        $config['max_size']             = 100;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('excelFile'))
        {
            $error = array('error' => $this->upload->display_errors('<strong>','</strong>'));
            $this->session->set_flashdata('error', $error['error']);
            redirect('olahdata','refresh');
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
        }
        // redirect(base_url('olahdata'));
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load('./uploads/'.$filename);

        $inputFileName = './uploads/'.$filename;
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();

        $this->showExcelData($sheetData);
    }

    public function showExcelData($sheetData){
        $this->Olahdata_model->simpan($sheetData);

        $data['user'] = $this->session->userdata('username');
        $data['page_active'] = 'olahdata';
        $data['dataTable'] = $this->Olahdata_model->tampilDataExcel();
        $data['tahunAjaran'] = $this->Olahdata_model->tampilTahunAjaran();
        
        $this->load->view('templates/header', $data);
        $this->load->view('olahdata/index', $data);
        $this->load->view('templates/footer');
    }

    public function proses(){

        $tahun_ajaran = $this->input->post("tahun_ajaran");
        $semester = $this->input->post("semester");

        $resultFkm = $this->Olahdata_model->prosesFKM();
        $this->simpan($tahun_ajaran, $semester);
    }

    public function simpan($ta, $sem){
        $this->Olahdata_model->saveAllToDetailRecord($ta, $sem);
        $this->Olahdata_model->simpanHasilOlahDataToRecord($ta, $sem);

        redirect(base_url("record"));
    }
}