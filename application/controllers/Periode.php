<?php
defined('BASEPATH') or exit('No direct script access allowed');
//Controller merupakan penghubung antara Model dengan View.
class Periode extends CI_Controller
{
    public function __construct(){

        Parent::__construct();
        $this->load->model('Periode_model');
    }

    public function index() //fungsi tampil
    {
        if($this->session->userdata('username') != ''){
            $data['user'] = $this->session->userdata('username');
            $data['page_active'] = 'periode';
            $data['all_periode'] = $this->Periode_model->tampilPeriode();

            $this->load->view('templates/header', $data);
            $this->load->view('periode/index', $data);
            $this->load->view('templates/footer');
        }else{
            redirect(base_url());
        }
    }

    public function tambah(){
        if($this->session->userdata('username') != ''){

            $dataPeriode['tahun'] = $this->input->post('tahun');
            $dataPeriode['semester'] = $this->input->post('semester');
            $this->Periode_model->tambahPeriode($dataPeriode);
            
            // inisial set_flash data untuk notifikasi
            $this->session->set_flashdata('status', 'berhasil');
            $this->session->set_flashdata('info', 'ditambahkan');
            $this->session->set_flashdata('colorInfo', 'success');
            // redirect ke profile
            redirect(base_url('periode'));
        }else{
            $this->session->set_flashdata('status', 'gagal');
            $this->session->set_flashdata('info', 'ditambahkan');
            $this->session->set_flashdata('colorInfo', 'danger');
            redirect(base_url('periode'));
        }
    }
}