<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct(){

        Parent::__construct();
        $this->load->model('User_model');
    }

    public function index(){
        if($this->session->userdata('username') != ''){

            $username = $this->session->userdata('username');
            $data['user'] = $username;
            $data['current_user'] = $this->User_model->currentUser($username);
            $data['all_user'] = $this->User_model->getAllUser();
            $data['page_active'] = 'profile';
            $this->load->view('templates/header', $data);
            $this->load->view('user/index', $data);
            $this->load->view('templates/footer');
        }else{
            redirect(base_url());
        }
    }

    public function add(){
        if($this->session->userdata('username') != ''){

            $tambahUser['npm'] = $this->input->post('npm');
            $tambahUser['password'] = $this->input->post('password');
            $tambahUser['fullname'] = $this->input->post('fullname');
            $tambahUser['konsentrasi'] = $this->input->post('konsentrasi');
            $this->User_model->addUser($tambahUser);
            
            // inisial set_flash data untuk notifikasi
            $this->session->set_flashdata('status', 'berhasil');
            $this->session->set_flashdata('info', 'ditambahkan');
            $this->session->set_flashdata('colorInfo', 'success');
            // redirect ke profile
            redirect(base_url('user'));
        }else{
            $this->session->set_flashdata('status', 'gagal');
            $this->session->set_flashdata('info', 'ditambahkan');
            $this->session->set_flashdata('colorInfo', 'danger');
            redirect(base_url('user'));
        }
    }

    public function delete($npm){
        if($this->session->userdata('username') != ''){

            if($npm == $this->session->userdata('username')){
                $this->session->set_flashdata('status', 'gagal');
                $this->session->set_flashdata('info', 'dihapus');
                $this->session->set_flashdata('colorInfo', 'danger');
                redirect(base_url('user'));
            }else{
                $this->User_model->deleteUser($npm);

                // inisial set_flash data untuk notifikasi
                $this->session->set_flashdata('status', 'berhasil');
                $this->session->set_flashdata('info', 'dihapus');
                $this->session->set_flashdata('colorInfo', 'success');
                // redirect ke profile
                // redirect ke profile
                redirect(base_url('user'));
            }
        }else{
            redirect(base_url());
        }
    }

    public function edit(){
        if($this->session->userdata('username') != ''){

            $editUser['npm'] = $this->session->userdata('username');
            $editUser['password'] = $this->input->post('password');
            $editUser['fullname'] = $this->input->post('fullname');
            $editUser['konsentrasi'] = $this->input->post('konsentrasi');
            $this->User_model->editUser($editUser);

            // inisial set_flash data untuk notifikasi
            $this->session->set_flashdata('status', 'berhasil');
            $this->session->set_flashdata('info', 'diubah');
            $this->session->set_flashdata('colorInfo', 'success');
            // redirect ke profile
            // redirect ke profile
            redirect(base_url('user'));
        }else{
            $this->session->set_flashdata('status', 'gagal');
            $this->session->set_flashdata('info', 'diubah');
            $this->session->set_flashdata('colorInfo', 'danger');
            redirect(base_url());
        }
    }
}