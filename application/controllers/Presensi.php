<?php
require_once('vendor/vendor/autoload.php');

defined('BASEPATH') or exit('No direct script access allowed');
//Controller merupakan penghubung antara Model dengan View.
class Presensi extends CI_Controller
{
    public function __construct(){

        Parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('Presensi_model');
    }

    public function index() //fungsi tampil
    {
        if($this->session->userdata('username') != ''){
            $data['user'] = $this->session->userdata('username');
            $data['page_active'] = 'presensi';
            $dataCount = $this->Presensi_model->cekdata();

            if($dataCount >= 1){
                $data['dataTable'] = $this->Presensi_model->tampilPresensi();

                $this->load->view('templates/header', $data);
                $this->load->view('presensi/index', $data);
                $this->load->view('templates/footer');
            }else{
                $this->load->view('templates/header', $data);
                $this->load->view('presensi/index', $data);
                $this->load->view('templates/footer');
            }
        }else{
            redirect(base_url());
        }
    }

    public function upload(){
        $filename = $_FILES['excelFile']['name'];
        // echo $filename;
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'xlsx';
        $config['max_size']             = 1000;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('excelFile'))
        {
            $error = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata('error', $error['error']);
            redirect('presensi','refresh');
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
        }
        // redirect(base_url('olahdata'));
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load('./uploads/'.$filename);

        $inputFileName = './uploads/'.$filename;
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();

        $this->showExcelData($sheetData);
    }

    public function showExcelData($sheetData){
        $this->Presensi_model->simpanPresensi($sheetData);

        $data['user'] = $this->session->userdata('username');
        $data['page_active'] = 'presensi';
        $data['dataTable'] = $this->Presensi_model->tampilPresensi(); //array
        
        $this->load->view('templates/header', $data);
        $this->load->view('presensi/index', $data);
        $this->load->view('templates/footer');
    }
}