<?php
require_once("application/asset/vendor/autoload.php");
defined('BASEPATH') or exit('No direct script access allowed');
//Controller merupakan penghubung antara Model dengan View.

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Record extends CI_Controller
{
    public function __construct(){
        Parent::__construct();
        $this->load->dbutil();
        $this->load->model('Record_model');
    }

    public function index()
    {
        if($this->session->userdata('username') != ''){
            $data['user'] = $this->session->userdata('username');
            $data['daftar_periode'] = $this->Record_model->tampilPeriode();
            $data['page_active'] = 'record';

            $this->load->view('templates/header', $data);
            $this->load->view('record/index', $data);
            $this->load->view('templates/footer');
        }else{
            redirect(base_url());
        }
    }

    public function detail($tahun1, $tahun2, $semester){
        if($this->session->userdata('username') != ''){
            $tahun_ajaran = $tahun1."/".$tahun2;
            $data['user'] = $this->session->userdata('username');
            $id_periode = $this->Record_model->getIdPeriode($tahun_ajaran, $semester);
            $data['data_periode'] = [
                "tahun" => $tahun_ajaran, 
                "semester" => $semester,
                "id_periode" => $id_periode[0]['id_periode']
            ];
            $data['daftar_record'] = $this->Record_model->tampilRecordDataByPeriode($tahun_ajaran, $semester);

            $data['page_active'] = 'record';
            $this->load->view('templates/header', $data);
            $this->load->view('record/detail');
            $this->load->view('templates/footer');
        }else{
            redirect(base_url());
        }
    }

    public function all($id_periode, $kode_makul){
        if($this->session->userdata('username') != ''){
            $data['user'] = $this->session->userdata('username');
            $data['detail_record'] = $this->Record_model->tampilAllRecord($id_periode, $kode_makul);
            $data['data_periode'] = $this->Record_model->tampilPeriodeById($id_periode);
            $data['page_active'] = 'record';

            $this->load->view('templates/header', $data);
            $this->load->view('record/all', $data);
            $this->load->view('templates/footer');

            // echo "<pre>";
            // print_r($data['data_periode']);
            // echo "</pre>";
        }else{
            redirect(base_url());
        }
    }

    public function downloadRecord($tahun1, $tahun2, $semester){
        $tahun_ajaran = $tahun1."/".$tahun2;
        $result = $this->Record_model->download($tahun_ajaran, $semester);
        if($result == "Tidak ada data"){
            $this->session->set_flashdata('status_download', 'gagal');
            redirect(base_url("record"));
        }else{
            $data = $result; //data from db

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue('A1','No.');
            $sheet->setCellValue('B1','Tahun Ajaran');
            $sheet->setCellValue('C1','Semester');
            $sheet->setCellValue('D1','Kode Matakuliah');
            $sheet->setCellValue('E1','Matakuliah');
            $sheet->setCellValue('F1','NPM');
            $sheet->setCellValue('G1','Nama Asisten');

            for ($index=0; $index < count($data); $index++) {
                $cell = $index+2;
                $sheet->setCellValue('A'.$cell,$index+1);
                $sheet->setCellValue('B'.$cell,$data[$index]['tahun_ajaran']);
                if($data[$index]['semester'] == 1){
                    $sheet->setCellValue('C'.$cell,"Ganjil");
                }else{
                    $sheet->setCellValue('C'.$cell,"Genap");
                }
                $sheet->setCellValue('D'.$cell,$data[$index]['kode_makul']);
                $sheet->setCellValue('E'.$cell,$data[$index]['nama_makul']);
                $sheet->setCellValue('F'.$cell,$data[$index]['npm']);
                $sheet->setCellValue('G'.$cell,$data[$index]['nama_asisten']);
            }

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            if($data[0]['semester'] == 1){
                header('Content-Disposition: attachment; filename="Asisten Terbaik '.$data[0]['tahun_ajaran'].' Ganjil.xlsx"');
            }else{
                header('Content-Disposition: attachment; filename="Asisten Terbaik '.$data[0]['tahun_ajaran'].' Genap.xlsx"');
            }

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');
        }
    }
}