<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fadev extends CI_Controller
{
    public function __construct(){
            parent::__construct();
            $this->load->helper(array('form', 'url'));
    }

    public function index(){
        if($this->session->userdata('username') != ''){
            $data['user'] = $this->session->userdata('username');
            $data['page_active'] = 'fadev';
            $this->load->view('templates/header', $data);
            $this->load->view('fa-dev/index');
            $this->load->view('templates/footer');
        }else{
            redirect(base_url());
        }
    }
}