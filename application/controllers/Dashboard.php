<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function index()
    {
        if($this->session->userdata('username') != ''){
            $data['user'] = $this->session->userdata('username');
            $data['page_active'] = 'dashboard';
            $this->load->view('templates/header', $data);
            $this->load->view('dashboard/index');
            $this->load->view('templates/footer');
        }else{
            redirect(base_url());
        }
    }
}