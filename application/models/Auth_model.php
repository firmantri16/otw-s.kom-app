<?php
//Model berhubungan langsung dengan Database
class Auth_model extends CI_model{
    public function verification($npm, $password){
        $this->db->select()->from('user');
        $this->db->where('npm', $npm);
        $this->db->where('password', $password);
        
        $result = $this->db->get()->result_array();
        
        return $result;
    }
}
?>