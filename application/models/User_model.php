<?php
//Model berhubungan langsung dengan Database
class User_model extends CI_model{
    public function currentUser($username){
        $this->db->select()->from('user');
        $this->db->where('npm', $username);
        
        $result = $this->db->get()->result_array();
        
        return $result;
    }

    public function getAllUser(){
        $result = $this->db->get('user')->result_array();
        
        return $result;
    }

    public function addUser($data){
        $dataInsert = array(
            'npm' => $data['npm'],
            'password' => $data['password'],
            'fullname' => $data['fullname'],
            'konsentrasi' => $data['konsentrasi']
        );

        $this->db->insert('user', $dataInsert);
    }

    public function deleteUser($npm){
        $this->db->where('npm', $npm);
        $this->db->delete('user');
    }

    public function editUser($data){
        $dataUpdate = array(
            'password' => $data['password'],
            'fullname' => $data['fullname'],
            'konsentrasi' => $data['konsentrasi']
        );

        $this->db->set('password', $data['password']);
        $this->db->set('fullname', $data['fullname']);
        $this->db->set('konsentrasi', $data['konsentrasi']);
        $this->db->where('npm', $data['npm']);
        $this->db->update('user');
    }
}