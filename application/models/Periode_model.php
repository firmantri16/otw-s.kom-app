<?php
//Model berhubungan langsung dengan Database
class Periode_model extends CI_model{
    public function tambahPeriode($data){
        $dataInsert = array(
            'tahun_ajaran' => $data['tahun'],
            'semester' => $data['semester']
        );

        $this->db->insert('periode', $dataInsert);
    }

    public function tampilPeriode(){
        $result = $this->db->get('periode')->result_array();
        
        return $result;
    }
}