<?php
//Model berhubungan langsung dengan Database
class Record_model extends CI_model{
    public function tampilPeriode(){
        $result = $this->db->get('periode')->result_array();
        
        return $result;
    }

    public function getIdPeriode($ta, $sem){
        $this->db->select('id_periode');
        $this->db->where('tahun_ajaran', $ta);
        $this->db->where('semester', $sem);
        $result = $this->db->get('periode')->result_array();
        return $result;
    }

    public function tampilAllRecord($id, $kode){
        $this->db->select('*');
        $this->db->where('id_periode', $id);
        $this->db->where('kode_makul', $kode);
        $this->db->order_by('nilai', 'ASC');
        $result = $this->db->get('detail_record')->result_array();
        return $result;
    }

    public function tampilPeriodeById($id){
        $this->db->select('*');
        $this->db->where('id_periode', $id);
        $result = $this->db->get('periode')->result_array();
        return $result;
    }

    public function tampilRecordDataByPeriode($tahun, $semester){
        //cek apakah data dengan tahun dan semester terpilih ada di tabel record.
        $data_record = "Tidak ada data";
        $result_getId = $this->db->query("SELECT id_periode FROM periode WHERE tahun_ajaran = '".$tahun."' AND semester = '".$semester."'")->result_array();
        $result_cek = $this->db->query("SELECT count(*) as cek FROM record WHERE id_periode = '".$result_getId[0]["id_periode"]."'")->result_array();
        if($result_cek[0]["cek"] >= 1){
            $data_record = $this->db->query("SELECT p.tahun_ajaran, p.semester, r.npm, r.nama_asisten, r.kode_makul, r.nama_makul FROM record r JOIN periode p ON p.id_periode = r.id_periode WHERE p.id_periode = '".$result_getId[0]["id_periode"]."'")->result_array();
            
            return $data_record;
        }else{
            return $data_record;
        }        
    }

    public function download($tahun, $semester){
        $result = "Tidak ada data";
        $result_getId = $this->db->query("SELECT id_periode FROM periode WHERE tahun_ajaran = '".$tahun."' AND semester = '".$semester."'")->result_array();
        $result_cek = $this->db->query("SELECT count(*) as cek FROM record WHERE id_periode = '".$result_getId[0]["id_periode"]."'")->result_array();
        if($result_cek[0]["cek"] >= 1){
            $sql = "SELECT p.tahun_ajaran, p.semester, r.npm, r.nama_asisten, r.kode_makul, r.nama_makul FROM record r JOIN periode p ON p.id_periode = r.id_periode WHERE p.id_periode = '".$result_getId[0]["id_periode"]."'";
            $result = $this->db->query($sql)->result_array();

            return $result;
        }else{
            return $result;
        }    
    }
}