<?php
//Model berhubungan langsung dengan Database
class Presensi_model extends CI_model{
    public function simpanPresensi($data){
        $this->db->empty_table('presensi');
        for ($i=0; $i < count($data) ; $i++) { 
            $dataSimpan = array(
                'npm' => $data[$i][0],
                'nama_asisten' => $data[$i][1],
                'nama_makul' => $data[$i][2],
                'jurusan' => $data[$i][3],
                'jumlah_presensi' => $data[$i][4],
            );
        
            $this->db->insert('presensi', $dataSimpan);
        }
    }

    public function tampilPresensi(){
        $result = $this->db->get('presensi')->result_array();
        
        return $result;
    }

    public function cekdata(){
        $result = $this->db->query("SELECT COUNT(*) FROM presensi")->result();

        return $result;
    }
}