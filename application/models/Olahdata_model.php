<?php
//Model berhubungan langsung dengan Database
class Olahdata_model extends CI_model{
    public function simpan($data){
        $this->db->empty_table('temporary');
        for ($i=0; $i < count($data) ; $i++) { 
            $dataSimpan = array(
                'kode_makul' => $data[$i][0],
                'nama_makul' => $data[$i][1],
                'npm' => $data[$i][2],
                'nama_asisten' => $data[$i][3],
                'penguasaan_materi' => $data[$i][4],
                'membimbing_praktikum' => $data[$i][5],
                'public_speaking' => $data[$i][6],
                'kedisiplinan' => $data[$i][7],
            );
        
            $this->db->insert('temporary', $dataSimpan);
        }
    }

    public function tampilTahunAjaran(){
        $this->db->select('tahun_ajaran');
        $this->db->from('periode');
        $this->db->group_by("tahun_ajaran");
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function tampilDataExcel(){
        $query = $this->db->get('temporary')->result_array(); 
        return $query;
    }

    public function prosesFKM(){
        $looping = 0;
        $statusCluster = "";
        $cluster_1 = [];
        $cluster_2 = [];
        $matrixEd = [];
        $newClusterPoint = [];
        $tempMemberCluster = [];
        $jmlhData = $this->db->count_all("temporary");
        // Tahapan :
        // 1. Menentukan k sebagai jumlah cluster yang ingin dibentuk. Menetapkan pusat cluster.
        $data = $this->db->get("temporary")->result_array();
        $cluster_1 = $data[0]; //pusat cluster awal c1
        $cluster_2 = $data[$jmlhData-1]; //pusat cluster awal c2
        
        //loop dari sini
        for ($i=0; $i < 100; $i++) {
            if($i == 0){ //artinya loop pertama
        // 2. Menghitung jarak setiap data ke pusat cluster menggunakan persamaan Eucledian Distance.
                $matrixEd = $this->EucledianDistance($cluster_1, $cluster_2, $data, $jmlhData);
            }else{
                $cluster_1 = $newClusterPoint[0];
                $cluster_2 = $newClusterPoint[1];
                $matrixEd = $this->EucledianDistance($cluster_1, $cluster_2, $data, $jmlhData);
            }
            
        // 3. Mengelompokkan data ke dalam cluster dengan jarak paling minimum.
            $finalMatrix = $this->findCluster($matrixEd, $jmlhData);
            
            if($i == 0){
                $tempMemberCluster = $finalMatrix;
            }else{
                //cek apakah ada data yang berpindah cluster.
                $checkMovingCluster = $this->checkMovingCluster($finalMatrix);
                $statusCluster = $checkMovingCluster;
                if($statusCluster == "Berbeda"){
                    $tempMemberCluster = $finalMatrix;
                }else{
                    break;
                }
            }

        // 4. Menghitung pusat cluster yang baru
            $memberCluster = $this->memberCluster($finalMatrix, $jmlhData);
            
            $getDataMemberCluster = $this->getDataMemberCluster($data, $memberCluster);
            $generateNewClusterPoint = $this->getNewClusterPoint($getDataMemberCluster);
            $newClusterPoint = $generateNewClusterPoint;
            
        // 5. Mengulangi langkah ke-2 sampai dengan langkah ke-4 hingga tidak ada data yang berpindah cluster.
        // NOTE : perulangan akan berhenti apabila perulangan habis atau tidak ada data yang berpindah cluster. artinya seluruh cluster sama dengan cluster sebelumnya
            // cekendproses
            $looping+=1;
        };

        //menampilkan seluruh data member yang berhak.
        $finalMemberCluster = $this->memberCluster($tempMemberCluster, $jmlhData);
        $mergeValues = $this->mergeValues($finalMemberCluster, $finalMatrix);
        $getFinalMemberCluster = $this->getFinalMemberCluster($data, $finalMemberCluster);
        $mergeFinalData = $this->mergeFinalData($mergeValues, $getFinalMemberCluster);
        $simpanHasil = $this->simpanHasilOlahData($mergeFinalData);
    }

    private function EucledianDistance($c1, $c2, $array, $index){
        $eudist_c1 = [];
        $eudist_c2 = [];
        for ($i=0; $i < $index; $i++) { 
            $rumus_c1 = sqrt(pow($array[$i]["penguasaan_materi"]-$c1["penguasaan_materi"],2) + pow($array[$i]["membimbing_praktikum"]-$c1["membimbing_praktikum"],2) + pow($array[$i]["public_speaking"]-$c1["public_speaking"],2) + pow($array[$i]["kedisiplinan"]-$c1["kedisiplinan"],2));
            $eudist_c1[$i] = round($rumus_c1, 2);
        }
        for ($i=0; $i < $index; $i++) { 
            $rumus_c2 = sqrt(pow($array[$i]["penguasaan_materi"]-$c2["penguasaan_materi"],2) + pow($array[$i]["membimbing_praktikum"]-$c2["membimbing_praktikum"],2) + pow($array[$i]["public_speaking"]-$c2["public_speaking"],2) + pow($array[$i]["kedisiplinan"]-$c2["kedisiplinan"],2));
            $eudist_c2[$i] = round($rumus_c2, 2);
        }
        $matrix["Terhadap C1"] = $eudist_c1;
        $matrix["Terhadap C2"] = $eudist_c2;

        return $matrix;
    }

    private function findCluster($arr, $index){
        $cluster = [];
        $matrix = $arr;
        for ($i=0; $i < $index; $i++) {
            if($matrix["Terhadap C1"][$i] < $matrix["Terhadap C2"][$i]){
                $cluster[$i] = 1;
            }else{
                $cluster[$i] = 2;
            }
        }
        $matrix["Cluster"] = $cluster;
        
        // insert into database table : temp_cluster.
        $this->db->empty_table("temp_cluster");
        for ($i=0; $i < sizeof($matrix["Cluster"]); $i++) { 
            $data = array(
                "nomor_data" => $i,
                "cluster" => $matrix["Cluster"][$i]
            );
            $this->db->insert("temp_cluster", $data);
        }

        return $matrix;
    }

    private function memberCluster($arr, $index){
        $arrBerhak = [];
        $arrTdkBerhak = [];
        for ($i=0; $i < $index; $i++) { 
            if($arr["Cluster"][$i] == 1){
                array_push($arrBerhak, $i);
            }else{
                array_push($arrTdkBerhak, $i);
            }
        }
        $matrix["Berhak"] = $arrBerhak;
        $matrix["Tidak Berhak"] = $arrTdkBerhak;
        
        return $matrix;
    }

    private function getDataMemberCluster($dt, $mc){
        $berhak = [];
        $tidakBerhak = [];
        for ($i=0; $i < sizeof($mc["Berhak"]); $i++) {
            $val = $mc["Berhak"][$i];
            $berhak[$i] = $dt[$val];
        }

        for ($i=0; $i < sizeof($mc["Tidak Berhak"]); $i++) {
            $val = $mc["Tidak Berhak"][$i];
            $tidakBerhak[$i] = $dt[$val];
        }

        $matrix["Berhak"] = $berhak;
        $matrix["Tidak Berhak"] = $tidakBerhak;

        return $matrix;
    }

    private function getNewClusterPoint($arr){
        $matrix = [];
        $loop = 1;
        
        foreach ($arr as $index => $status) {
            $p_materi = 0;
            $m_prak = 0;
            $p_speaking = 0;
            $kedisiplinan = 0;
            for ($i=0; $i < sizeof($status); $i++) { 
                $p_materi += $status[$i]["penguasaan_materi"];
                $m_prak += $status[$i]["membimbing_praktikum"];
                $p_speaking += $status[$i]["public_speaking"];
                $kedisiplinan += $status[$i]["kedisiplinan"];
            }
            $pusatBaru["penguasaan_materi"] = round($p_materi/sizeof($status),2);
            $pusatBaru["membimbing_praktikum"] = round($m_prak/sizeof($status),2);
            $pusatBaru["public_speaking"] = round($p_speaking/sizeof($status),2);
            $pusatBaru["kedisiplinan"] = round($kedisiplinan/sizeof($status),2);

            array_push($matrix, $pusatBaru);
        }

        return $matrix;
    }

    private function checkMovingCluster($arr){
        $status = "";
        $arrStatus = [];
        $temp = $this->db->get("temp_cluster")->result_array();
        
        for ($i=0; $i < sizeof($arr["Cluster"]); $i++) { 
            if($arr["Cluster"][$i] == $temp[$i]["cluster"]){
                array_push($arrStatus, "Tidak Berpindah");
            }else{
                array_push($arrStatus, "Berpindah");
            }
        }
        for ($i=0; $i < sizeof($arrStatus); $i++) { 
            if($i < sizeof($arrStatus)-1){
                if($arrStatus[$i] == $arrStatus[$i+1]){
                    $status = "Sama";
                }else{
                    $status = "Berbeda";
                    break;
                }
            }
        }

        return $status;
    }

    private function getFinalMemberCluster($dt, $mc){
        $berhak = [];
        $tidakBerhak = [];
        for ($i=0; $i < sizeof($mc["Berhak"]); $i++) {
            $val = $mc["Berhak"][$i];
            $berhak[$i] = $dt[$val];
        }

        $matrix["Berhak"] = $berhak;

        return $matrix;
    }

    private function mergeValues($final, $temp){
        $tempValue = [];
        for ($i=0; $i < sizeof($final["Berhak"]); $i++) { 
            $index = $final["Berhak"][$i];
            $tempValue[$i] = $temp["Terhadap C1"][$index];
        }
        
        return $tempValue;
    }

    private function mergeFinalData($mrg, $mc){
        $matrix = $mc["Berhak"];
        
        for ($i=0; $i < sizeof($mrg); $i++) { 
            $matrix[$i]["nilai"] = $mrg[$i];
        }

        return $matrix;
    }

    private function simpanHasilOlahData($result){
        $this->db->empty_table("olahdata");
        for ($i=0; $i < sizeof($result); $i++) { 
            $data = [
                "kode_makul" => $result[$i]["kode_makul"],
                "nama_makul" => $result[$i]["nama_makul"],
                "npm" => $result[$i]["npm"],
                "nama_asisten" => $result[$i]["nama_asisten"],
                "penguasaan_materi" => $result[$i]["penguasaan_materi"],
                "membimbing_praktikum" => $result[$i]["membimbing_praktikum"],
                "public_speaking" => $result[$i]["public_speaking"],
                "kedisiplinan" => $result[$i]["kedisiplinan"],
                "nilai" => $result[$i]["nilai"]
            ];
            $this->db->insert("olahdata", $data);
        }
    }

    private function olahPresensi(){
        $exist = [];
        $presensi = [];
        $asistenTerbaik = [];
        $terpilih = "";
        $result = $this->db->query("SELECT * FROM `olahdata` WHERE nilai = (SELECT min(nilai) FROM `olahdata`)")->result_array();

        $jumlah = sizeof($result);
        if($jumlah > 1){
            for ($i=0; $i < $jumlah; $i++) { 
                $hasil = $this->db->query("SELECT count(npm) as 'status' FROM presensi WHERE npm = '".$result[$i]["npm"]."'")->result_array();
                $exist[$i] = $hasil[0];
            }
            $sql = "SELECT npm, max(jumlah_presensi) FROM presensi WHERE ";
            for ($i=0; $i < $jumlah; $i++) { 
                if($exist[$i]["status"] == 1){
                    $sql .= "npm = '".$result[$i]["npm"]."' OR ";
                }
            }
            $sql = trim($sql, "OR ");
            $terpilih = $this->db->query($sql)->result_array();
            $npmTerpilih = $terpilih[0]["npm"];
            for ($i=0; $i < $jumlah; $i++) { 
                if($result[$i]["npm"] == $npmTerpilih){
                    $asistenTerbaik = $result[$i];
                    break;
                }
            }

            
        }else{
            $asistenTerbaik = $result[0];
        }

        return $asistenTerbaik;
    }

    private function getIdPeriode($tahun, $sem){
        $result = $this->db->query("SELECT id_periode FROM periode WHERE tahun_ajaran='".$tahun."' AND semester='".$sem."'");

        return $result->result_array();
    }

    public function simpanHasilOlahDataToRecord($tahun_ajaran, $semester){
        $asistenTerbaik = $this->olahPresensi();
        $id_periode = $this->getIdPeriode($tahun_ajaran, $semester);
        $data = [
            "kode_makul"=> $asistenTerbaik["kode_makul"],
            "nama_makul"=> $asistenTerbaik["nama_makul"],
            "npm"=> $asistenTerbaik["npm"],
            "nama_asisten"=> $asistenTerbaik["nama_asisten"],
            "nilai"=> $asistenTerbaik["nilai"],
            "id_periode" => $id_periode[0]['id_periode']
        ];
        $this->db->insert("record", $data);
    }

    public function saveAllToDetailRecord($tahun_ajaran, $semester){
        $olahdata = $this->db->get('olahdata')->result_array();
        
        //simpan ke detail record.
        $id_periode = $this->getIdPeriode($tahun_ajaran, $semester);
        for ($x=0; $x < count($olahdata); $x++) { 
            $isExist = $this->db->query("SELECT count(npm) as cek FROM `detail_record` where kode_makul = '".$olahdata[$x]["kode_makul"]."' and npm = '".$olahdata[$x]["npm"]."' and id_periode = '".$id_periode[0]['id_periode']."'")->result_array();
            if($isExist[0]['cek'] > 0){
                //status : ada. maka data yang tersimpan di tabel detail record, dihapus, diganti dengan yang baru
                //hapus data yang lama.
                $deleteData = [
                    "kode_makul" => $olahdata[$x]["kode_makul"],
                    "npm" => $olahdata[$x]["npm"],
                    "id_periode" => $id_periode[0]['id_periode']
                ];
                $delete = $this->db->delete('detail_record', $deleteData);

                // ganti yang baru
                $data = [
                    "kode_makul" => $olahdata[$x]["kode_makul"],
                    "nama_makul" => $olahdata[$x]["nama_makul"],
                    "npm"=> $olahdata[$x]["npm"],
                    "nama_asisten"=> $olahdata[$x]["nama_asisten"],
                    "penguasaan_materi"=> $olahdata[$x]["penguasaan_materi"],
                    "membimbing_praktikum"=> $olahdata[$x]["membimbing_praktikum"],
                    "public_speaking"=> $olahdata[$x]["public_speaking"],
                    "kedisiplinan"=> $olahdata[$x]["kedisiplinan"],
                    "nilai"=> $olahdata[$x]["nilai"],
                    "id_periode" => $id_periode[0]['id_periode']
                ];
                
                $this->db->insert("detail_record", $data);
            }else{
                //status : tidak ada. maka langsung disimpan.
                $data = [
                    "kode_makul" => $olahdata[$x]["kode_makul"],
                    "nama_makul" => $olahdata[$x]["nama_makul"],
                    "npm"=> $olahdata[$x]["npm"],
                    "nama_asisten"=> $olahdata[$x]["nama_asisten"],
                    "penguasaan_materi"=> $olahdata[$x]["penguasaan_materi"],
                    "membimbing_praktikum"=> $olahdata[$x]["membimbing_praktikum"],
                    "public_speaking"=> $olahdata[$x]["public_speaking"],
                    "kedisiplinan"=> $olahdata[$x]["kedisiplinan"],
                    "nilai"=> $olahdata[$x]["nilai"],
                    "id_periode" => $id_periode[0]['id_periode']
                ];
                
                $this->db->insert("detail_record", $data);
            }
        }
    }
}